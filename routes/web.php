<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', "HomeController@index");
Route::get('/', "HomeController@index");
Route::get('/accueil',"HomeController@index");
//Route::get('/backoffice',"BackofficeController@backoffice");
Route::get('/categories', 'CategoriesController@index')->name('categories.index');
Route::get('categories/create','CategoriesController@create')->name("categories.create");
Route::post('categories/create','CategoriesController@store')->name("categories.store");

Route::get('category/{id}', 'CategoriesController@destroy');
Route::delete('category/{id}', 'CategoriesController@destroy');
Route::delete('product/{id}', 'ProductsController@destroy');

Route::get('categories/{id}/edit','CategoriesController@edit')->name("editer_category");

Route::patch('categories/{id}/edit', 'CategoriesController@update')->name('update_category');


Route::get('/products','ProductsController@index')->name('products.index');
Route::get('/products/create','ProductsController@create')->name('product_create');//->middleware('auth');
Route::post('/products/create','ProductsController@store')->name('store_products');

Route::get('products/{id}/edit','ProductsController@edit')->name("editer_produit");

Route::patch('products/{id}/edit', 'ProductsController@update')->name('update_product');

Route::get('about',function(){
    return view("about");
});

Route::get('service',function(){
    return view("service");
});

Route::get('contact',function(){
    return view("contact");
});

Route::get('carte',function(){
    return view("carte");
});
Route::get('payement',function(){
    return view("payement");
});
Route::get('compte',function(){
    return view("compte");
});
Route::get('souhait',function(){
    return view("souhait");
});
Route::get('detail',function(){
    return view("detail");
});

Route::get('shop',function(){
    return view("shop");
});
Route::get('/abonnement/expired', "AbonnementController@expired");


Route::get('/seller',"LoginController@redirectTo");

Route::get('/admin/dashboard',"LoginController@redirectTo");
//
/*
//Route::get('/produits',"ProductsController@index");
//Route::get('/produits/{slug}',"ProductsController@show");
/*


Route::get('/produits/{id}', function ($id) {
    return "je suis le produit $id";
});
Route::get('/produits', function ($id) {
    return "je suis le produit $id";
});*/

Route::get('/welcome', 'HomeController@index')->name('welcome');

Auth::routes(['verify'=>true]);

//Route::get('/home', 'HomeController@index');
Route::get("/produit/{id}/show", 'ProductsController@show');

Route::get('connexion',function(){
    return view("connexion");
});

Route::get('inscription',function(){
    return view("inscription");
});


Route::post('/ajout_category', 'AjaxController@ajout_category');

Route::post('/product/add_to_cart', "AjaxController@add_to_cart");

Route::get('/cart', "OrderController@cart");

Route::get('/checkout', 'OrderController@checkout');
Route::get('/success_cart', 'OrderController@/success_cart');




