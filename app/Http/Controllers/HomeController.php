<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\product;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
        //$this->middleware('verified');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = product::all()->take(6);//paginate(6);
       // dd($products);
       /* foreach($products as $product){
            dd($product->prix);
        }*/
        return view('home',compact('products'));
    }

}
