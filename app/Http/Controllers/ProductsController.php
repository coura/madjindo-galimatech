<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class ProductsController extends Controller
{

    public function __construct()
    {
       // $this->middleware(['auth','can:admin'])->except(['category','show','cart','checkout']);
    }

    public function uploadImage(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null){
        $name = !is_null($filename) ? $filename : str_random('25');
        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }


    public function show($id){
        $product = Product::find($id);
        return view("products.show", compact('product'));
    }


    public function create()
    {
        $categories = \App\category::pluck('name','id');
        return view('products.create', compact('categories'));
        return redirect()->route('products')->with(['success' => "Produit enregistré"]);
       // $produit->user_id  = Auth::id();
        $produit->save();
    }


    public function edit($id)
    {
        $product = \App\product::find($id);
        $categories = \App\category::pluck('name','id');
        return view('products.edit', compact('product','categories'));
        $this->authorize('admin');

    }


    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name'   => 'required',
            'prix' => 'required | numeric',
            'product_image' => 'nullable | image | mimes:jpeg,png,jpg,gif | max:2048'
        ]);

        $product = \App\product::find($id);
        if($product){
            if($request->has('product_image')){
                //On enregistre l'image dans une variable
                $image = $request->file('product_image');
                if(file_exists(public_path().$product->images))//On verifie si le fichier existe
                    Storage::delete(asset($product->images));//On le supprime alors
                //Nous enregistrerons nos fichiers dans /uploads/images dans public
                $folder = '/uploads/images/';
                $image_name = Str::slug($request->input('name')).'_'.time();
                $product->images = $folder.$image_name.'.'.$image->getClientOriginalExtension();
                //Maintenant nous pouvons enregistrer l'image dans le dossier en utilisant la méthode uploadImage();
                $this->uploadImage($image, $folder, 'public', $image_name);
            }

            $product->update([

                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'description' => $request->input('description'),
                'category_id' => $request->input('category_id'),

            ]);
        }
        return redirect()->back();
    }

    public function index(){
        $products = product::orderBy('created_at')->get();
        return view('products.index', compact('products'));
    }
    public function store(Request $request)
    {
        $produit = new Product();
        $data = $request->validate([
            'name'=>'required|min:5',
            'prix' => 'required|max:9999999|numeric',
            'quantite' => 'required|max:700|numeric',
            'description' => 'max:1000000',
        "product_image" => 'nullable | image | mimes:jpeg,png,jpg,gif | max: 2048']);

        if($request->has('product_image')){
            //On enregistre l'image dans un dossier
            $image = $request->file('product_image');
            //Nous allons definir le nom de notre image en combinant le nom du produit et un timestamp
            $image_name = Str::slug($request->input('name')).'_'.time();
            //Nous enregistrerons nos fichiers dans /uploads/images dans public
            $folder = '/uploads/images/';
            //Nous allons enregistrer le chemin complet de l'image dans la BD
            $produit->images = $folder.$image_name.'.'.$image->getClientOriginalExtension();
            //Maintenant nous pouvons enregistrer l'image dans le dossier en utilisant la methode uploadImage();
            $this->uploadImage($image, $folder, 'public', $image_name);
            $products = \App\Product::orderBy('created_at', 'DESC')->get();
        }


        $produit->name = $request->input('name');
        $produit->prix = $request->input('prix');
        $produit->category_id = $request->input('category_id');
        $produit->quantite = $request->input('quantite');
        $produit->description = $request->input('description');
        $produit->save();
        return redirect('/products');



    }
    public function destroy($id)
    {
        $category = \App\product::find($id);
        if($category)
            $category->delete();
        return redirect()->route('products.index');
    }




}
