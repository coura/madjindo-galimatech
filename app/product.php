<?php

namespace App;
 use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $fillable = ['name','id_category','name','description','prix','quantite'];

    public function category(){
        return $this->belongsTo('App\category');
    }

    public static function boot(){
   parent::boot();
   static::saving(function($model){
       $model->slug = Str::slug($model->name);
   });
   static::saving(function($model){
       $model->slug = Str::slug($model->name);
   });

}

}
