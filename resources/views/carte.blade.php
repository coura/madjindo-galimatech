@extends("layouts.design")
@section("content")
<!-- End Top Search -->

<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Cart</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Cart</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                           
                        </tr>
                        </thead>
                        <tbody>
                 
                        <tr>
                           
                            <td scope="col">pack 400ml </td>
                            <td scope="col">500</td>
                            <td scope="col">1</td>
                            
                            <td scope="col">500 F CFA</td>
                            
                        </tr>
                   

                        <tr class="out_button_area">
                            <td colspan="3"> </td>
                            <td>
                                <div class="checkout_btn_inner d-flex align-items-center">
                                    <a class="btn btn-primary" href="/checkout">Confirmer la commande</a>
                                </div>
                            </td>
                        </tr>
</tbody>
                    </table>
                </div>
            </div>
        </div>

       
       

    </div>
</div>
<!-- End Cart -->

<!-- Start Instagram Feed  -->

<!-- End Instagram Feed  -->

@endsection
<!-- Start Footer  -->
