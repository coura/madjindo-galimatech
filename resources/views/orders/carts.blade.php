@extends("layouts.design")
@section("content")


<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Cart</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Cart</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                           
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantite</th>
                            <th>Total</th>
                           
                        </tr>
                        </thead>

                  <tbody>
                  @foreach($cart['products'] as $panier)
                        <tr>
                           <!-- <td scope="col">{{$panier['name']}}</td>
                            <td scope="col">{{$panier['prix']}}</td>
                            <td scope="col">{{$panier['quantite']}}</td>
                            <td scope="col">{{$panier['total']}}F CFA</td>-->
                        </tr>
                    @endforeach

                        <tr class="out_button_area">
                            <td colspan="3"> </td>
                            <td>
                                <div class="checkout_btn_inner d-flex align-items-center">
                                    <a class="btn btn-primary" href="/checkout">Confirmer la commande</a>
                                </div>
                            </td>
                        </tr>
</tbody>
                     
                    </table>
                </div>
            </div>
        </div>

@endsection