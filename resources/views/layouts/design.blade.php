
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Madjindo </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/goutte.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/icon-goute.jpg">

    <script src="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></script>
    <link href="{{asset('css/all.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/ajustement.css')}}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Start Main Top -->
<div class="main-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="text-slid-box">
                    <div id="offer-box" class="carouselTicker">
                        <ul class="offer-box">
                            <li>
                                <i img src="{{asset('images/back.jpg')}}" class="fab fa-opencart"></i> Madjindo un nouveau produit sur le marche
                            </li>
                            <li>
                                <i img src="{{asset('images/back.jpg')}}" class="fab fa-opencart"></i> Il y'a maintenant plusieurs mois nous nous sommes lancé dans la production,
                            </li>
                            <li>
                                <i img src="{{asset('images/back.jpg')}}" class="fab fa-opencart"></i> la distribution d'une eau filtré et purifié
                            </li>
                            <li>
                                <i img src="{{asset('images/back.jpg')}}" class="fab fa-opencart"></i> des packs de 400ml disponible
                            </li>
                            <li>
                                <i img src="{{asset('images/back.jpg')}}" class="fab fa-opencart"></i> des packs de 250ml disponibles
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> pour 10packs achetes un packs offert
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> une eau pure provenant des plus profondes nappes de Thies
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> Madjindo la qualite de l'eau
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <div class="right-phone-box">
                    <p>Call US :- <a href="#"> +221 33 825 32 20</a></p>
                </div>
                <div class="our-link">
                    <ul>
                        <li><a href="connexion">LOGIN</a></li>
                        <li><a href="inscription">REGISTER</a></li>
                        <li><a href="contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main Top -->

<!-- Start Main Top -->
<header class="main-header">
    <!-- Start Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
        <div class="container-fluid">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="home"><img width="25%" src="{{asset('images/rezi.png')}}" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="nav-item active"><a class="nav-link" href="home">Acceuil</a></li>
                   <!-- <li class="nav-item"><a class="nav-link" href="about">A Propos</a></li>-->
                    <li class="dropdown megamenu-fw">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Minerales</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Eau</h6>
                                        <div class="content">
                                            <ul class="menu-col" f>
                                                <li><a href="shop">Pack 400ml</a></li>
                                                <li><a href="shop">Pack 250ml</a></li>
                                                <li><a href="shop">Boutielle 30cl</a></li>
                            
                                                <li><a href="shop">Bouteille 1L</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                    <!--<div class="col-menu col-md-3">
                                        <h6 class="title">Arachide</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="shop">Sucre</a></li>
                                                <li><a href="shop">Sale</a></li>
                                                <li><a href="shop">Chips</a></li>
                                                <li><a href="shop">Amuse bouche</a></li>
                                            </ul>
                                        </div>
                                    </div>-->
                                    <!-- end col-3 -->
                                   <!-- <div class="col-menu col-md-3">
                                        <h6 class="title">Nekstech</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="shop">Diiman ERP</a></li>
                                                <li><a href="shop">Geolocalisation</a></li>
                                                <li><a href="shop">Cyber-Securite</a></li>

                                            </ul>
                                        </div>
                                    </div>-->
                                  <!--  <div class="col-menu col-md-3">
                                        <h6 class="title">Autres</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="shop">...</a></li>
                                                <li><a href="shop">...</a></li>

                                            </ul>
                                        </div>
                                    </div>-->
                                    <!-- end col-3 -->
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown megamenu-fw">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Agroalimentaires</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Arachide</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="shop">Sucre</a></li>
                                                <li><a href="shop">Sale</a></li>
                                                <li><a href="shop">Chips</a></li>
                                                <li><a href="shop">Chocolat</a></li>
                                            </ul>
                                        </div>
                                    </div>


                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>

                     <li class="dropdown megamenu-fw">
                        <a href="#" class="nav-link dropdown-toggle arrow" >Tableau de bord</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Access Administrateur</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                               <li><a href="/categories/create">Creer categories</a></li>
                                                <li><a href="/category/edit">Lister categories</a></li>
                                                
                                                
                                                <li><a href="/products/create">Creer produits</a></li>
                                                <li><a href="/products">Lister produits</a></li>
                                                
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown ">
                        <a href="#" class="nav-link dropdown-toggle arrow" >Technique</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Logiciel</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                               <li><a href="shop">Diiman ERP</a></li>
                                                <li><a href="shop">Geolocalisation</a></li>
                                                <li><a href="shop">Cyber-Securite</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>
       <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow"  data-toggle="dropdown">Logement meubles</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">LM</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                               <li><a href="https://www.booking.com/hotel/sn/rezilux.fr.html">Reservation</a></li>
                                                <li><a href="https://www.youtube.com/watch?v=xLxw6bsCGYI">Tutoriel d'ajout d' hebergement</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>
                    <!-- <li class="dropdown megamenu-fw">
                        <a href="www.madjindo.com" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Logements Meubles</a>
                         <ul class="dropdown-menu">
                             <li><a href="https://www.booking.com/hotel/sn/rezilux.fr.html">Reservation</a></li>
                             <li><a href="https://www.youtube.com/watch?v=xLxw6bsCGYI">Tutoriel d'ajout d' hebergement</a></li>
                         </ul>
                    </li>-->
                     <li class="dropdown">
                         <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Achats</a>
                         <ul class="dropdown-menu">
                             <li><a href="carte">Carte</a></li>
                             <li><a href="payement">Payement</a></li>
                            <!-- <li><a href="compte">Mon Compte</a></li>
                             <li><a href="souhait">Liste de Souhaits</a></li>
                             <li><a href="detail">Details Achats</a></li>-->
                         </ul>
                     </li>
                   <!--  <li class="nav-item"><a class="nav-link" href="service">Nos Service</a></li>
                     <li class="nav-item"><a class="nav-link" href="contact">Contactez Nous</a></li>-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->

            <!-- Start Atribute Navigation -->

            <!-- End Atribute Navigation -->
        </div>
        <!-- Start Side Menu -->
        <div class="side">
            <a href="#" class="close-side"><i class="fa fa-times"></i></a>
            <li class="cart-box">
                <ul class="cart-list">
                    <li>
                        <a href="#" class="photo"><img src="{{asset('images/img-pro-01.jpg')}}" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Delica omtantur </a></h6>
                        <p>1x - <span class="price">$80.00</span></p>
                    </li>
                    <li>
                        <a href="#" class="photo"><img src="{{asset('images/img-pro-02.jpg')}}" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Omnes ocurreret</a></h6>
                        <p>1x - <span class="price">$60.00</span></p>
                    </li>
                    <li>
                        <a href="#" class="photo"><img src="{{asset('images/img-pro-03.jpg')}}" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Agam facilisis</a></h6>
                        <p>1x - <span class="price">$40.00</span></p>
                    </li>
                    <li class="total">
                        <a href="#" class="btn btn-default hvr-hover btn-cart">VIEW CART</a>
                        <span class="float-right"><strong>Total</strong>: $180.00</span>
                    </li>
                </ul>
            </li>
        </div>
        <!-- End Side Menu -->
    </nav>
    <!-- End Navigation -->
</header>
<!-- End Main Top -->

<!-- Start Top Search -->
<div class="top-search">
    <div class="container">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" class="form-control" placeholder="Search">
            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
        </div>
    </div>
</div>


<div class="container">
    @yield("content")
</div>

<div class="instagram-box" id="taille">
    <div class="main-instagram owl-carousel owl-theme">
        <div class="item">
            <div class="ins-inner-box">
                <img width="200%" src="{{asset('images/thies2.jpg')}}" alt="" />

            </div>
        </div>

        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/thies4.jpg')}}" alt="" />

            </div>
        </div>

        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/thies6.jpg')}}" alt="" />

            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/thies7.jpg')}}" alt="" />

            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/thies8.jpg')}}" alt="" />

            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/madjindoThies.jpg')}}" alt="" />

            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img height="200%"  width="100%" src="{{asset('images/thies4.jpg')}}" alt="" />

            </div>
        </div>

    </div>
</div>

<footer>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-widget">
                        <h4>About REZILUX</h4>
                        <p> REZILUX est une startup 100% Sénégalaise qui évolue dans l'immobilier, les services NTIC et solutions informatiques digitales de management d’entreprise basées sur le Cloud et l’intelligence artificielle entres autres...

En tant qu’éditeur d’applications et de solutions de gestion digitale, notre ambition repose entièrement sur la satisfaction de notre prestigieuse clientèle.

Notre ambition pour les prochaines années peut se résumer en un mot : votre satisfaction. Bien au-delà d’un simple slogan publicitaire, c’est notre crédo !


                        </p>
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-link">
                        <h4>Information</h4>
                        <ul>
                            <li><a href="about">About Us</a></li>
                            <li><a href="service">Nos Service</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                            <li><a href="http://rezilux.com/">Terms &amp; Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Delivery Information</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-link-contact">
                        <h4>Contact Us</h4>
                        <ul>
                            <li>
                                <p><i class="fas fa-map-marker-alt"></i>Address: Michael I. Days 3756 <br>Preston Street Wichita,<br> KS 67213 </p>
                            </li>
                            <li>
                                <p><i class="fas fa-phone-square"></i>Phone: <a href="tel:(00221) 33 820 56 21">(00221) 77 416 16 15</a></p>
                            </li>
                            <li>
                                <p><i class="fas fa-envelope"></i>Email: <a href="info@rezilux.com">rezilux@gmail.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Start copyright -->
<div class="footer-copyright">
    <p class="footer-company">All Rights Reserved. &copy; 2018 <a href="#">Madjindo</a> Design By :
        <!--<a href="https://html.design/">html design</a></p>-->
    <a href="https://madjindo.com/">madjindo web</a></p>

</div>
<!-- End copyright -->

<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

<!-- ALL JS FILES-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- ALL PLUGINS -->
<script src="{{asset('js/jquery.superslides.min.js')}}"></script>
<script src="{{asset('js/bootstrap-select.js')}}"></script>
<script src="{{asset('js/inewsticker.js')}}"></script>
<script src="{{asset('js/bootsnav.js')}}"></script>
<script src="{{asset('js/images-loded.min.js')}}"></script>
<script src="{{asset('js/isotope.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/baguetteBox.min.js')}}"></script>
<script src="{{asset('js/form-validator.min.js')}}"></script>
<script src="{{asset('js/contact-form-script.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/app copy.js')}}"></script>

<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea.description',
        width: 900,
        height: 300
    });
</script>


</body>

</html>
