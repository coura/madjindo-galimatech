<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('login/images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/vendor/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('login/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/vendor/select2/select2.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('login/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{asset('login/images/img-01.png') }}" alt="IMG">
				</div>
                <form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
        <div class="card-header login100-form-title">{{ __('Register') }}</div>
        @csrf
        <div class="wrap-input100 validate-input">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror input100" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{__('name')}}">
            <span class="focus-input100"></span><span class="symbol-input100"><i class="fa fa-envelope" aria-hidden="true"></i></span>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="wrap-input100 validate-input">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror input100" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}">
                <span class="focus-input100"></span><span class="symbol-input100"><i class="fa fa-at" aria-hidden="true"></i></span>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="wrap-input100 validate-input">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror input100" name="password" required autocomplete="new-password" placeholder="{{__('Password')}}">
            <span class="focus-input100"></span><span class="symbol-input100"><i class="fa fa-user-lock" aria-hidden="true"></i></span>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="wrap-input100 validate-input">
                <input id="password-confirm" type="password" class="form-control input100" name="password_confirmation" required autocomplete="new-password" placeholder="{{__('Confirm Password')}}"><span class="focus-input100"></span><span class="symbol-input100"><i class="fa fa-user-lock" aria-hidden="true"></i></span>
        </div>

        <div class="form-group row mb-0">
            <div class="container-login100-form-btn">
                <button type="submit" class="btn btn-primary login100-form-btn">
                    {{ __('Register') }}
                </button>
            </div>
        </div>
        <div class="text-center p-t-12">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </div>
    </form>
    
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{asset('login/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{asset('login/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{asset('login/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{asset('login/vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{asset('login/vendor/tilt/tilt.jquery.min.js') }}"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('login/js/main.js') }}"></script>

</body>
</html>