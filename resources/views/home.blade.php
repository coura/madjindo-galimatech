@extends("layouts.design")
@section("content")
<!-- End Top Search -->
<!-- Start Slider -->
<link href="{{asset('css/ajustement.css')}}" rel="stylesheet">
<div id="slides-shop" class="cover-slides">
    <ul class="slides-container">
        <li class="text-left">
            <img src="{{asset('images/mangrove5.webp')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Bienvenue A <br> Madjindo</strong></h1>
                        
                    </div>
                </div>
            </div>
        </li>
        <li class="text-center">
            <img src="{{asset('images/mangrove6.webp')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Bienvenue A <br> Madjindo</strong></h1>
                        
                    </div>
                </div>
            </div>
        </li>
        <li class="text-right">
            <img src="{{asset('images/mangrove7.webp')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Bienvenue A <br> Madjindo</strong></h1>
                       
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <div class="slides-navigation">
        <a href="#" class="next"><i class="fas fa-arrow-right"></i></i></a>
        <a href="#" class="prev"><i class="fas fa-arrow-right"></i></i></a>
    </div>
</div>

<!-- Start Blog -->
<div class="latest-blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-all text-center">
                    <h1>Liste de souhaits</h1>
                   
                    <div class="row">
                    @foreach($products as $product)
                        <div class="col-lg-4 col-sm-6 portfolio-item">
                            <div class="card h-100">
                                <a href="#"><img class="card-img-top" src="{{$product->images ?? asset('uploads/images/default.png')}}" height="250" width="250" alt=""></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="/produit/{{$product->id}}/show">{{$product->name}}</a>
                                    </h4>
                                    <p class="card-text">{!! \Illuminate\Support\Str::words($product->description, 25,'....')  !!}</p>
                                </div>
                            </div>
                                    <form action="#" id="{{'product_'.$product->id}}" class="add-to-cart">
                                @csrf
                                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="hidden" name="prix" value="{{$product->prix}}">
                                <button type="submit" class="btn btn-primary btn-fancy" href="/product/{{$product->id}}/show">Ajouter au panier</button>
                                </form>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
       

       <!-- <div class="row">
            <div class="col-md-6 col-lg-4 col-xl-4">
                <div class="blog-box">
                    <div class="blog-img">
                        <img class="img-fluid" src="{{asset('images/blog-img.jpg')}}" alt="" />
                    </div>
                    <div class="blog-content">
                        <div class="title-blog">
                            <h3>Fusce in augue non nisi fringilla</h3>
                            <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                        </div>
                        <ul class="option-blog">
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xl-4">
                <div class="blog-box">
                    <div class="blog-img">
                        <img class="img-fluid" src="{{asset('images/blog-img-01.jpg')}}" alt="" />
                    </div>
                    <div class="blog-content">
                        <div class="title-blog">
                            <h3>Fusce in augue non nisi fringilla</h3>
                            <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                        </div>
                        <ul class="option-blog">
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xl-4">
                <div class="blog-box">
                    <div class="blog-img">
                        <img class="img-fluid" src="{{asset('images/blog-img-02.jpg')}}" alt="" />
                    </div>
                    <div class="blog-content">
                        <div class="title-blog">
                            <h3>Fusce in augue non nisi fringilla</h3>
                            <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                        </div>
                        <ul class="option-blog">
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>
<!-- End Blog -->


<!-- Start Instagram Feed -->

<!-- End Instagram Feed -->

<!-- Start Footer -->
<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<!--<script>
let form = document.getElementById("category_form");
$('form.add-to-cart').submit(function (e) {
   e.preventDefault();
   let form_data = $(this).serialize();
   //alert(form_data);
   $.ajax({
       type: "POST",
       url: '/product/add_to_cart',
       data: form_data,
       success: function (data) {
           if(data.success){
               $(`form#${data.id} button`).html('produit ajouter au panier');
               alert("Produit ajoute au panier");
               Swal.fire({
                   title: 'Produit Ajouté dans le panier!',
                   text: 'Do you want to continue',
                   icon: 'success',
                   html:'<a href="/cart" class="btn btn-success">Ouvrir le panier</a>',
                   showCloseButton: true,
                   showCancelButton: false,
                   focusConfirm: false,
                   confirmButtonText:
                       '<i class="fa fa-thumbs-up"></i> Ajouter d\'autres produits!',
                   confirmButtonAriaLabel: 'Ajouter d\'autres produits!'
               });
           }else{
               console.log("il y une erreur");
           }
       }
   })
});
</script>-->
@endsection
