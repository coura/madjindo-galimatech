@extends("layouts.design")
@section("content")
    @if(session("success"))
        <div class="alert alert-success" >{{session('success')}}</div>
        @endif
<table class="table table-striped">
<div class="container">

    <form>
    <tr>
                <th>Nom Categorie</th>  <br><br >
    </tr>
    @foreach($categories as $category)
        <tr>

            <th>{{$category->name}}</th>

            <th>

           <p><a class="btn btn-primary" href="{{route('editer_category',['id'=>$category->id])}}">Editer</a>


</p></th>
           <th> <form action="category/{{$category->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger" name="delete" value="Supprimer">
            </form></th>
        </tr>
    @endforeach
    </form>
    </div>
</table>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <script defer>
        let form = document.getElementById("category_form");
        form.addEventListener('submit', function (e) {
            e.preventDefault();
            let donnees_formulaire = $(this).serialize();
            $.ajax({
                type: "POST",
                url: '/ajout_category',
                data: donnees_formulaire,
                success: function(data){
                    alert("Category ajoutée");
                    console.log(donnees_formulaire);
                    $("#category_table").append(`<tr> <td>#</td> <td> ${data.data.name} </td> <td> <div class="row justify-content-end"> <div class="col"><a href="/categories/${data.data.id}/edit" class="btn btn-primary">Editer</a></div> <form class="col" action="/categories/${data.data.id}" method="post"> <input type="hidden" name="_token" value="{{@csrf_token()}}"> <input type="hidden" name="_method" value="delete">                            <button type="submit" class="btn btn-danger">Suppimer</button> </form> </div> </td> </tr>`);
                    $('#add_category').modal('hide');
                }
            });
        })
    </script>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>


@endsection

