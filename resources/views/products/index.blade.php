@extends("layouts.design")
@section("content")
<table class="table table-striped">
    <tr>
               <th>Produit</th>       <th>Nom Produit</th>    <th>Prix Produit</th>           <th>quantite</th> <th>description</th>
    </tr>
    @foreach($products as $product)
        <tr>
            <th>
            <img src="{{$product->images ? asset($product->images) : asset('uploads/images/default.png')}}" alt="{{$product->name}}" width="50">
            </th>
            <th>{{$product->name}}</th>
            <th>{{$product->prix}} {{ $product->category->name ?? '' }}</th>
            <th>{{$product->quantite}}</th>
            <th>{{$product->description}}</th>
           
            <th>

                <p><a  class="btn btn-primary"  href="{{route('editer_produit',['id'=>$product->id])}}">Editer</a>
                </p>
            </th>
            <th> <form action="category/{{$product->id}}" method="post">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger" name="delete" value="Supprimer">
                </form>
            </th>

        </tr>
    @endforeach
</table>
@endsection
