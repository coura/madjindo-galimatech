@extends("layouts.design")
@section("content")
<div class="container">
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">{{$error}}</div>
        @endforeach
    @endif
    <div><p><a class="btn btn-primary"  href="{{route('products.index')}}">{{__('Enregistrement d\'un produit')}}</a></p></div>
    <form action="{{route('store_products')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <input type="text" name="name" class="form-control" placeholder="le nom du produit">
        </div>
        <div><input type="file" name="product_image" class="form-control"></div>

        <div>
            <input type="text" name="prix" class="form-control" placeholder="Le prix du produit">
        </div>
        <div>
            <select name="category_id" id="category_id" class="form-control">
                <option value=""></option>
                @foreach($categories as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>




        <div>
            <input type="text" name="quantite" class="form-control" placeholder="La quantite du produit">
        </div>
        <div>
           <!-- <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="La description"></textarea>-->
            <textarea class="description" id="description" name="description" cols="30" rows="10" class="form-control" placeholder="La description"></textarea>
        </div>
        <div>
            <button class="btn btn-primary">Enregistrer</button>
        </div>
    </form>
</div>
@endsection
